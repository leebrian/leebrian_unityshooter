﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Boundary
{
    public float xMin, xMax, yMin, yMax, zMin, zMax;
}
public class PlayerController1 : MonoBehaviour
{
    private Rigidbody rb;
    public float speed;
    public float tilt;
    public float frontTilt;
    public Boundary boundary;

    public GameObject shot;
    public GameObject shot2;
    public Transform shotSpawn;

    public float fireRate;
    private float nextFire;

    private AudioSource audioSource;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        audioSource = GetComponent<AudioSource>();
    }

    void Update()
    {
        if(Input.GetButton("Fire1") && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            //            GameObject clone =
            Instantiate(shot, shotSpawn.position, shotSpawn.rotation); // as GameObject;

            audioSource.Play();
        }
        if(Input.GetButton("Fire2") && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            Instantiate(shot2, shotSpawn.position, shotSpawn.rotation);

            audioSource.Play();
        }
    }

    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis( "Horizontal");
        float moveVertical = Input.GetAxis( "Vertical");

        Vector3 movement = new Vector3(moveHorizontal, moveVertical, 0.0f);
        rb.velocity = movement * speed;

        rb.position = new Vector3(
            Mathf.Clamp(rb.position.x, boundary.xMin, boundary.xMax),
            Mathf.Clamp(rb.position.y, boundary.yMin, boundary.yMax),
            0.0f            
        );

        rb.rotation = Quaternion.Euler(rb.velocity.y * -frontTilt, 0.0f, rb.velocity.x * -tilt);
    }
}