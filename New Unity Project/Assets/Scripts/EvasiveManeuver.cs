﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EvasiveManeuver : MonoBehaviour {

    public float dodge;
    public float smoothing;
    public float tilt;
    public float frontTilt;

    public Vector2 startWait;
    public Vector2 maneuverTimex;
    public Vector2 maneuverWaitx;
    public Vector2 maneuverTimey;
    public Vector2 maneuverWaity;
    public Boundary boundary;

    private float currentSpeed;
    private float targetManeuverx;
    private float targetManeuvery;
    private Rigidbody rb;

	void Start ()
    {
        rb = GetComponent<Rigidbody>();
        currentSpeed = rb.velocity.z;
        StartCoroutine(Evade());
	}
	IEnumerator Evade()
    {
        yield return new WaitForSeconds(Random.Range(startWait.x, startWait.y));

        while (true)
        {
            targetManeuverx = Random.Range(1, dodge) * -Mathf.Sign (transform.position.x);
            targetManeuvery = Random.Range(1, dodge) * -Mathf.Sign(transform.position.y);
            yield return new WaitForSeconds(Random.Range(maneuverTimex.x, maneuverTimex.y));
            yield return new WaitForSeconds(Random.Range(maneuverTimey.x, maneuverTimey.y));
            targetManeuverx = 0;
            targetManeuvery = 0;
            yield return new WaitForSeconds(Random.Range(maneuverWaitx.x, maneuverWaitx.y));
            yield return new WaitForSeconds(Random.Range(maneuverWaity.x, maneuverWaity.y));
        }
    }
	void FixedUpdate ()
    {
        float newManeuverx = Mathf.MoveTowards(rb.velocity.x, targetManeuverx, Time.deltaTime * smoothing);
        float newManeuvery = Mathf.MoveTowards(rb.velocity.y, targetManeuvery, Time.deltaTime * smoothing);
        rb.velocity = new Vector3(newManeuverx, newManeuvery, currentSpeed);
        rb.position = new Vector3
        (
            Mathf.Clamp(rb.position.x, boundary.xMin, boundary.xMax),
            Mathf.Clamp(rb.position.y, boundary.yMin, boundary.yMax),
            Mathf.Clamp(rb.position.z, boundary.zMin, boundary.zMax)            
        );
        rb.rotation = Quaternion.Euler(rb.velocity.y * -frontTilt, 0.0f, rb.velocity.x * -tilt);
	}
}
